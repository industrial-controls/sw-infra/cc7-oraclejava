FROM cern/cc7-base

ARG JAVA_PACKAGE=java-11-openjdk-headless.x86_64

ENV JAVA_HOME /usr/lib/jvm/jre
ENV LANG C.UTF-8

# Works for JDK and JRE installations
ENV PATH $PATH:$JAVA_HOME/jre/bin:$JAVA_HOME/bin

RUN yum-config-manager --enable epel jpackage && \
    yum update -y && yum install -y ${JAVA_PACKAGE} rsync && \
    yum install -y CERN-CA-certs && \
    yum clean all
